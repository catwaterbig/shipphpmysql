<?php 
	include 'connectdb.php';

	if ($_SERVER["REQUEST_METHOD"] === "GET"){
		  if (isset($_GET["calendar_send"]) && isset($_GET["from_save"]) && isset($_GET["to_save"])){
		    $calendar = $_GET["calendar_send"];
		    $fromSave = $_GET["from_save"];
		    $toSave = $_GET["to_save"];

		    //Create Table If Not Exit
		    $result = $DB->execute("CREATE TABLE ship_head(".
									"id INT NOT NULL AUTO_INCREMENT,".
									"sdate DATETIME,".
									"send_from VARCHAR(5),".
									"send_to VARCHAR(5),".
									"mod_date DATETIME,".
									"PRIMARY KEY (id)".
								   	") DEFAULT CHARSET = utf8");

		    //Create Table If Not Exit
		    $resultdetail= $DB->execute("CREATE TABLE ship_detail(".
 										"id INT NOT NULL AUTO_INCREMENT,".
 										"hid int,".
 										"sdesc VARCHAR(50),".
 										"amount INT,".
 										"mod_date DATETIME,".
 										"PRIMARY KEY (id),".
 										"FOREIGN KEY (hid) REFERENCES ship_head(id)".
										") DEFAULT CHARSET = utf8;");

		    //Insert DB
		    $insert = $DB->execute("INSERT INTO ship_head(sdate,send_from,send_to,mod_date) VALUES(STR_TO_DATE('$calendar','%d-%m-%Y'),'$fromSave','$toSave',NOW())");
			echo (json_encode($insert));
		  }
	 }

?>