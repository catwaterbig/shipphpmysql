//Web Module
var shipApp = angular.module('shipApp', ['ngRoute']);

//Route
shipApp.config(function($routeProvider) {  
	$routeProvider
		.when('/', {
			templateUrl: 'list_ship_head.html',
			controller: 'ListShipControl'
		})
		.when('/save', {
			templateUrl: 'save_ship_head.html',
			controller: 'SaveShipController'
		})
		.when('/save/:savehid', {
			templateUrl: 'save_ship_head.html',
			controller: 'SaveShipController'
		})
		.otherwise({ redirectTo: '/' });
});

//Control
shipApp.controller('ListShipControl', ['$scope', '$http', function($scope, $http) {

	//Select all list data ship head 
	$http
		.get('db/selectdata.php', {val:"pui"})
		.success(function (data, status, headers, config){
			console.log(data);
			//var res = angular.fromJson(response);
			//var data = res.data;
			//var day = moment("2016-07-22 00:00:00");
			//console.log(day.format("DD/MM/YYYY"));
			for(var i = 0; i < data.length; i++){
				data[i].sdate = moment(data[i].sdate).format("DD/MM/YYYY");
			}

			$scope.shipCollection = angular.fromJson(data);
		})
		.error(function (data, status, headers, config){
			//alert("No data");
		});

	//Add New Ship Head
	$scope.addNewShipHead = function(){
		//Go to page add new
	};

	//Delet List page
	$scope.deletedata = function(id_ship){
		//delete data from tbody
		for (var i = 0; i < $scope.shipCollection.length; i++) {
			$id = $scope.shipCollection[i].haid;
			
			if($id == id_ship){
				$scope.shipCollection.splice(i, 1);
				break;
			}
		}

		var params = {id_delete:id_ship};
		var config = {params: params};


		$http
			.get('db/deletedata.php', config)
			.success(function (data, status, headers, config){
				console.log(data);

			})
			.error(function (data, status, headers, config){
				alert("Error");
			});
	};



}]);



shipApp.controller('SaveShipController', ['$scope', '$http', '$routeParams', function($scope, $http, $routeParams){
	console.log($routeParams);

		//Init
	$scope.hid = 0;
	$scope.lockfrom = false;
	$scope.shipCollection = [];
	$scope.shipDetailDes = "";
	$scope.shipDetailAmount = 0;
	$scope.from_save  = "KKN";
	$scope.to_save = "KKN";
	var status_new = true; 
	$scope.sdesc = "";
	$scope.amount = 0;


	//Check Object Empty
	if(!Object.keys($routeParams).length == 0 && $routeParams.constructor === Object){
		//button -> Edit
		$scope.hid = $routeParams["savehid"];

		var config = {params: $routeParams};
		status_new = false;

		$http
			.get('db/selectdata.php', config)
			.success(function (data, status, headers, config){
				console.log(data);
				setDate(data.sdate);
				$scope.from_save = data.send_from;
				$scope.to_save = data.send_to;
			})
			.error(function (data, status, headers, config){
				alert("Error");
			});

	}else{
		//button -> Add new list
		//Load Max Id
		status_new = true;
		$http
			.get('db/maxid.php')
			.success(function (data, status, headers, config){
				$scope.hid = parseInt(data.max_id)+1;
			})
			.error(function (data, status, headers, config){
				//alert("No data");
				$scope.hid = 1;
			});

		setDate(new Date());
	}
	
	//Save Data Ship
	$scope.saveData = function(hid, from_save, to_save){
		var calendar_send = $("#datepicker").data('date');
		console.log("hid::"+hid);
		//Save Data
		var params = {hid:hid, calendar_send:calendar_send, from_save:from_save, to_save:to_save};
		var config = {params: params};

		var url_shiphead = 'db/'+((status_new)?'insertshiphead.php':'updateshiphead.php');

		$http
			.get(url_shiphead, config)
			.success(function (data, status, headers, config){
				//console.log(data);
				$scope.lockfrom = true;
				//If press button Edit
			})
			.error(function (data, status, headers, config){
				alert("Error1");
			});

		//Read Detail if press Edit button
		if(!status_new){
			//If press button Save
			$scope.hid = $routeParams["savehid"];
			var config = {params: $routeParams};

			$http
			.get('db/selectshipdetail.php', config)
			.success(function (data, status, headers, config){
				console.log(data);
				$scope.lockfrom = true;
				//$scope.shipCollection.push({shipDetailDes:data.sdesc, shipDetailAmount:data.amount});
				$scope.shipCollection = data;
			})
			.error(function (data, status, headers, config){
				alert("Error2");
			});
			
		}
	}

	//Add Detail Ship
	$scope.addShipDetail = function(hid, sdesc, amount){
		
		if(sdesc != "" && amount != "" && amount > 0){
			$scope.shipCollection.push({sdesc:sdesc, amount:amount});
			//Add Data Ship Detail
			var params = {hid:hid, sdesc:sdesc, amount:amount};
			var config = {params: params};

			$http
			.get('db/insertshipdetail.php', config)
			.success(function (data, status, headers, config){
				console.log(data);
				$scope.sdesc = "";
				$scope.amount = 0;
			})
			.error(function (data, status, headers, config){
				alert("Error");
			});
		}else{
			alert("Value incorrect");
		}

		
	}

	//Delete detail desc 
	$scope.deletedata = function(id_ship_detail){
		//delete data from tbody
		for (var i = 0; i < $scope.shipCollection.length; i++) {
			$id = $scope.shipCollection[i].id;
			
			if($id == id_ship_detail){
				$scope.shipCollection.splice(i, 1);
				break;
			}
		}

		var params = {id_delete:id_ship_detail};
		var config = {params: params};


		$http
			.get('db/deletedatadetail.php', config)
			.success(function (data, status, headers, config){
				console.log(data);

			})
			.error(function (data, status, headers, config){
				alert("Error");
			});
		

	};


	//Function setDate
	function setDate(dateVal){
		$('#datepicker').datetimepicker({
	    	format: 'DD-MM-YYYY',
	    	defaultDate: dateVal
	 	});
	}

}]);
