<?php 
	include 'connectdb.php';

	if ($_SERVER["REQUEST_METHOD"] === "GET"){
		  if (isset($_GET["hid"]) && isset($_GET["sdesc"]) && isset($_GET["amount"])){
		    $hid = $_GET["hid"];
		    $detailDes = $_GET["sdesc"];
		    $detailAmount = $_GET["amount"];

		    //Create Table If Not Exit
		    $result = $DB->execute("CREATE TABLE ship_head(".
									"id INT NOT NULL AUTO_INCREMENT,".
									"sdate DATETIME,".
									"send_from VARCHAR(5),".
									"send_to VARCHAR(5),".
									"mod_date DATETIME,".
									"PRIMARY KEY (id)".
								   	") DEFAULT CHARSET = utf8");

		    //Create Table If Not Exit
		    $resultdetail= $DB->execute("CREATE TABLE ship_detail(".
 										"id INT NOT NULL AUTO_INCREMENT,".
 										"hid int,".
 										"sdesc VARCHAR(50),".
 										"amount INT,".
 										"mod_date DATETIME,".
 										"PRIMARY KEY (id),".
 										"FOREIGN KEY (hid) REFERENCES ship_head(id)".
										") DEFAULT CHARSET = utf8;");

		    //Insert DB
		    $insert = $DB->execute("INSERT INTO ship_detail(hid,sdesc,amount,mod_date) VALUES($hid,'$detailDes', $detailAmount, NOW())");
			echo (json_encode($insert));
		  }
	 }

?>